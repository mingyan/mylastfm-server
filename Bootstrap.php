<?php
use myapp\service;
class Bootstrap
{
    protected $_appConfig;

	public function __construct()
	{
		$this->_setupIncludePath()
			->_setupAutoload()
			->_loadConfig()
			->_setupModel();

	}

	protected function _loadConfig()
	{
		$env = getenv('APPLICATION_ENV');
        if (empty($env)) {
			$env = 'dev';
            //throw new \Exception('Undefined application envrionment. Please specify the environment variable APPLICATION_ENV.');
        }
        $configFilePath = __DIR__ . '/config/' . strtolower($env) . '.php';
        if (!file_exists($configFilePath)) {
            throw new \Exception('Failed to load the applicaton configuration file under ' . $configFilePath);
        }

		$this->_appConfig = require_once($configFilePath);

		return $this;
	}

	protected function _setupIncludePath()
	{
		$paths = explode(PATH_SEPARATOR, get_include_path());
		$extraPaths = array(SITE_PATH . '/library',
						SITE_PATH . '/model',
						SITE_PATH . '/controller',
						);
		$includePath = array_merge($paths, $extraPaths);
		set_include_path(implode(PATH_SEPARATOR, $includePath));


		return $this;
	}

	protected function _setupAutoload()
	{
		spl_autoload_register(function($className){
            spl_autoload($className);
		});
        //Custom autoloader to work around the spl_autoload issue with the lower case class file names
        //in PHP 5.6.14
        spl_autoload_register(function($className){
            $includePaths = explode(PATH_SEPARATOR, get_include_path());
            $classFilePath = str_replace('\\', '/', $className) . '.php';

            foreach($includePaths as $path) {
                $fullClassFilePath = $path . '/' . $classFilePath;
                if (file_exists($fullClassFilePath)) {
                    require_once($fullClassFilePath);
                    return;
                }
            }
            throw new \LogicException('Class file ' . $fullClassFilePath
                . ' not found in the include path. ');
        });

		return $this;
	}

	protected function _setupModel()
	{
		//Artist model set up
        $lastFmConfig = $this->_appConfig['last_fm'];
        $lastFm = new \myapp\service\LastFm();
        $lastFm->setApiKey($lastFmConfig['api_key'])
            ->setFormat($lastFmConfig['format'])
            ->setRootUrl($lastFmConfig['root_url']);
        \myapp\Model::setDAO('Artist', $lastFm);

		//Track model set up
		\myapp\Model::setDAO('Track', $lastFm);

	}


}