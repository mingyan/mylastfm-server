<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Simple last.fm search app</title>
    <meta name="description" content="A little last.fm search app!">
    <meta name="author" content="ming">
    <link rel="stylesheet" href="/css/style.css?v=1.0">
</head>
<body>
<?php
echo $content;
?>
<script src="/js/app.js"></script>
</body>
</html>