<?php
return [
    'last_fm' => [
        'format' => 'json',
        'api_key' => '1f13dae823f02f8d045f0622c861fe9f',
        'root_url' => 'http://ws.audioscrobbler.com/2.0/',
        'enable_cache' => true,
        'cache_driver' => 'file',
        'cache_path' => dirname(dirname(__FILE__)) . '/data/cache',
    ],
];
