﻿<?php
header("Access-Control-Allow-Origin: *");

//Enable error reporting for dev environment
if (strtolower(getenv('APPLICATION_ENV')) === 'dev') {
	error_reporting(E_ALL);
}

if (!defined('SITE_PATH')) {
	define('SITE_PATH', realpath(dirname(dirname(__FILE__))));
}

require_once(SITE_PATH . '/Bootstrap.php');

$bootstrap = new Bootstrap();
try {
	$frontController = new myapp\FrontController();
	$frontController->setPath(SITE_PATH . '/controller');
	$frontController->dispatch();
} catch (Exception $e) {
    $errorController = new \ErrorController();
    $errorController->setError($e);
    $errorController->indexAction();

}