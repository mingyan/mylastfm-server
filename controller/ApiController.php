<?php
use myapp\BaseController;
use myapp\Model;
class ApiController extends BaseController
{

    public function indexAction(){
        echo json_encode(array('version' => '0.0.0.1', 'info' => 'API Controller here.'));
    }

    public function getTopArtistsAction()
    {
        $countryName = $this->_getParam('country');
        $page = $this->_getParam('page');

        if (empty($page)) {
            $page = 1;
        }

        $model = Model::get('Artist');
        $result = $model->fetchByCountryName($countryName, $page, 5);

        header('Content-type: text/json');

        //Caching the result for 1hr
        header("Cache-Control: max-age=3600");
        echo json_encode($result);
    }

    public function getTopTracksAction()
    {
        $artistName = $this->_getParam('artist');
        $page = $this->_getParam('page');

        if (empty($page)) {
            $page = 1;
        }

        $model = Model::get('Track');
        $result = $model->fetchTopTracksByArtist($artistName, $page, 5);

        header('Content-type: text/json');
        header("Access-Control-Allow-Origin: *");
        //Caching the result for 1hr
        header("Cache-Control: max-age=3600");
        echo json_encode($result);
    }
}