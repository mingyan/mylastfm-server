<?php
use myapp\BaseController;
use myapp\Model;

class ErrorController extends BaseController
{
    protected $_error;
    public function setError($e)
    {
        $this->_error = $e;
    }
    public function indexAction()
    {
        $this->_view->errorMessage = $this->_error->getMessage();
        $this->_view->render('error/index');
    }
}