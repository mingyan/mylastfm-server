<?php
class Track extends BaseModel
{
    protected $_dAO;

    /**
     * Fetch top tracks by a artist name
     * @param string $artistName
     * @param int $page current page number
     * @param int $perpage limit the result perpage
     * @return array
     */
    public function fetchTopTracksByArtist($artistName, $page = 1, $perpage = 5)
    {
        $dao = $this->_dAO;
        $dao->setMethod('artist.getTopTracks')
            ->setPage($page)
            ->setPerpage($perpage)
            ->setFormat('json')
            ->setExtraQueryParams(['artist' => $artistName]);
        $result = $dao->execute();
        return $result;
    }
}