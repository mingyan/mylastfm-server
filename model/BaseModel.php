<?php
abstract class BaseModel
{
    protected $_dAO;

    /**
     * Inject the DAO
     */
    function __construct($dao)
    {
        if (empty($dao)) {
            throw new \Exception('Error: empty DAO was given.');
        }
        $this->_dAO = $dao;
    }

}