<?php
class Artist extends BaseModel
{
    protected $_dAO;

    /**
     * Fetch artists by country
     * @param string $countryName from ISO 3166-1
     * @param int $page current page number
     * @return array
     */
    public function fetchByCountryName($countryName, $page = 1, $perpage = 5)
    {
        $dao = $this->_dAO;
        $dao->setMethod('geo.getTopArtists')
            ->setPage($page)
            ->setPerpage($perpage)
            ->setFormat('json')
            ->setExtraQueryParams(['country' => $countryName]);
        $result = $dao->execute();
        return $result;
    }


}