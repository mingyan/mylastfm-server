This app is a simple API server for the last.fm search app.

Features including

* Powered by a custom mini MVC framework
* Using a simple dependency injection for a better testable model. Test files to be implemented
* Generic Error handling
* Using layout files for a better separation of views
* Caching in Http header
* Web service calls with cURL
* Support PHP 5.3 for name space and class autoloading
* Application configuration supports the environment switch by using the APPLICATION_ENV server variable

Known issues to be addressed
* Need to make it more configurable by moving the user configurable values into the config file.
* Need to create test file to improve the code coverage for the app.
* Need to improve the UI with more css to make it nicer

For PHP 5.4+, you can test this app by running the PHP built-in server in the 'public' folder

APPLICATION_ENV=dev php -S 127.0.0.1:3000


Apache vhost file. Please update the path/name in the [] accordingly for your set up.

<VirtualHost *:80>
    ServerAdmin mingyan214@gmail.com
    ServerName [mylastfm.dev]
    DocumentRoot "[/Users/mingyan/Projects/mylastfm/public]"
    ErrorLog "[/Users/mingyan/Projects/mylastfm/data/log/mylastfm-error_log]"
    <Directory "[/Users/mingyan/Projects/mylastfm/public]">
        DirectoryIndex index.php index.html
        AllowOverride all
        Options -MultiViews
        Order allow,deny
        Allow from all
    </Directory>

</VirtualHost>

Directory structure
- config : config files
- controller: Controller classes
- data: file cache and log files
- library: application library files
- public: web document root with css, js and image files
- view: view script and layouts
- Bootstrap: Application bootstrap


