<?php
namespace myapp;
class Model
{
	/**
	* @var data access object for models
	*/
	protected static $_dAO;

    protected static $_dAORegistry = array();
	
	/**
	* Loads the model class
	* @param string $modelName model class name
	*/
	public static function get($modelName)
	{
		if (empty($modelName)) {
			throw new \Exception('Error: empty model name found.');
		}

        //If a DAO is registered, inject the DAO into the model
        if (array_key_exists($modelName, self::$_dAORegistry)) {
            return new $modelName(self::$_dAORegistry[$modelName]);
        }

        //else return the model only
//		$modelPath = SITE_PATH . '/model/' . $modelName . '.php';
//
//		if (!is_readable($modelPath)) {
//			throw new \Exception('Error: invalid model file. ' . $modelPath);
//		}
//
//		require_once($modelPath);
		return new $modelName();
	}
	
	/**
	* Set up the DAO class for data access for models
	*/
	public static function setDAO($modelName, $daoObject)
	{
        if (!is_object($daoObject)) {
            throw new \Exception('Error: invalid dao object.');
        }

        self::$_dAORegistry[$modelName] = $daoObject;
		return;
	}
}