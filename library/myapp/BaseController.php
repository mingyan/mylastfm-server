<?php
namespace myapp;
use myapp\View;

abstract class BaseController
{
	protected $_view;
	
	function __construct()
	{
		$this->_view = new View();
        $this->_view->setLayout('default');
		
	}
	abstract function indexAction();

    /**
     * Returns the parameter in GET or POST request
     *
     * @param $name
     * @return null
     */
    protected function _getParam($name)
    {
        $param = null;

        if(isset($_REQUEST[$name])){
            $param = $this->_filter($_REQUEST[$name]);
        }else if(isset($_GET[$name])){
            $param = $this->_filter($_GET[$name]);
        }else if(isset($_POST[$name])){
            $param = $this->_filter($_POST[$name]);
        }
        return $param;
    }

    protected function _filter($value)
    {
        return strip_tags(trim($value));
    }


}