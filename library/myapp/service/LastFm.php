<?php
namespace myapp\service;
class LastFm
{

    protected $_rootUrl;
    protected $_apiKey;
    protected $_perpage;
    protected $_method;
    protected $_page = 1;
    protected $_cachedResult = [];
    protected $_format;
    protected $_extraQueryParams = [];

    public function setRootUrl($rootUrl)
    {
        $this->_rootUrl = $rootUrl;
        return $this;
    }

    public function setApiKey($apiKey)
    {
        if (empty($apiKey)) {
            throw new \Exception('Error: empty api key was given.');
        }
        $this->_apiKey = $apiKey;
        return $this;
    }

    public function setPerPage($numberPerPage)
    {
        if (empty($numberPerPage)) {
            throw new \Exception('Error: empty number of items per page was given.');
        }
        $this->_perpage = (int) $numberPerPage;
        return $this;
    }

    public function setMethod($method)
    {
        if (empty($method)) {
            throw new \Exception('Error: empty method was given.');
        }
        $this->_method = $method;
        return $this;
    }

    /**
     * @param $params Custom parameters for specific API calls
     */
    public function setExtraQueryParams(array  $params)
    {
        if (!empty($params)) {
            $this->_extraQueryParams = $params;
        }
        return $this;
    }

    /**
     *Set the page number for pagination
     */
    public function setPage($page)
    {
        $this->_page = $page;
        return $this;
    }

    public function setFormat($format)
    {
        $this->_format = $format;
        return $this;
    }

    /**
     * Execute the query
     */
    public function execute()
    {
        $url = $this->_generateUrl();
        $response = $this->_sendRequest($url);
        return $response;

    }

    protected function _generateUrl()
    {
        $url = $this->_rootUrl . '?';
        $mainParams = array(
            'method' => $this->_method,
            'api_key' => $this->_apiKey,
            'format' => $this->_format,
            'limit' => $this->_perpage,
            'page' => $this->_page,
        );
        $fullQueryParams = array_merge($mainParams, $this->_extraQueryParams);
        $url .= http_build_query($fullQueryParams);

        return $url;

    }

    protected function _sendRequest($url)
    {

        $ch = curl_init() ;
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 60,      // timeout on connect
            CURLOPT_TIMEOUT        => 60,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => true
        );

        curl_setopt_array( $ch, $options );

        $output = curl_exec($ch) ;

        curl_close($ch);

        if($output){

            if($this->_format == 'json') {

                return json_decode($output);
            }

            throw new \Exception('Error: Unknown response format.');

        }else{
            throw new \Exception('Error: No response from server.');
        }

    }
}