<?php
namespace myapp;
class View
{
	protected $_vars = array();
	protected $_controllerName;
    protected $_layout;
	
	/**
	* Set undefined variables in the view
	*/
	public function __set($key, $value)
	{
		$this->_vars[$key] = $value;
	}
	
	public function render($viewscriptName = 'index')
	{
		$viewFilePath = SITE_PATH . '/view/' . $viewscriptName . '.php';
		if (!file_exists($viewFilePath)) {
			throw new \Exception('Error: viewscript file not found in ' . $viewFilePath);
		}

        ob_start();

        $content = '';
        extract($this->_vars, EXTR_OVERWRITE);
        include($viewFilePath);
        $content = ob_get_contents();

        ob_end_clean();

        $layout = '';
        if($this->_layout){
            $path = SITE_PATH .'/view/layout/' .$this->_layout .'.php';
            if(file_exists($path)){
                include_once($path);
            } else {
                throw new \Exception('Error: Invalid Layout ' . $this->_layout . '  was given.');
            }
        }

        $html = ob_get_contents();
        ob_end_clean();

        echo $html;

        exit();
	}

    public function setLayout($layoutName)
    {
        $this->_layout = $layoutName;
        return $this;

    }

    public function getLayout()
    {
        return $this->_layout;
    }


}