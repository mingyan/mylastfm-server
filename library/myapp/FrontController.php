<?php
namespace myapp;

class FrontController
{
	private $_controllerPath;
	private $_controllerFileName;
	private $_actionName;
	private $_controllerName;
	
	/**
	* Set the controller path
	*
	* @param string $path Controller directory path
	*/
	public function setPath($path) 
	{
		if (is_dir($path) === false) {
			throw new \Exception('Error: invalid controller path: ' . $path);
		}
		$this->_controllerPath = $path;
		return $this;
	}
	
	/**
	* Execute the controller/action
	*/
	public function dispatch()
	{
		$this->_parseUri();
		
		//Instantiate the controller class
		$controllerClassName = $this->_controllerName . 'Controller';
        if (!class_exists($controllerClassName)) {
            throw new \Exception('Error: failed to load the controller: ' . $controllerClassName);
        }
		$controller = new $controllerClassName();

		if (!method_exists($controller, $this->_actionName)) {
			throw new \Exception('Error: calling an undefined action: ' . $this->_actionName);
		}
		
		$controller->{$this->_actionName}();
		
	}
	
	/**
	* Get the controller name and the action name from the route string
	*/
	private function _parseUri()
	{
        $controller = 'index';
        $action = 'index';

        //Default route
        if (!isset($_SERVER['REQUEST_URI'])) {
            $this->_controllerName = $controller;
            $this->_actionName = $action;
            return;
        }

        //Resolve the controller name and the action anme from the rt
        $route = $_SERVER['REQUEST_URI'];
        $parts = explode('/', $route);

        if (empty($parts[1])) {
            $this->_controllerName = 'Index';
        } else {
            $this->_controllerName = $this->_dashToCamelCase($parts[1], false);
        }
        if (empty($parts[2])) {
            $this->_actionName = 'indexAction';
        } else {
            if (stripos($parts[2], '?') !== false) {
                $this->_actionName = $this->_dashToCamelCase(substr($parts[2], 0, strpos($parts[2], '?'))). 'Action';
            } else {
                $this->_actionName = $this->_dashToCamelCase($parts[2], true) . 'Action';
            }

        }

        return;
	}
	
	/**
	* Converts the controller name/action name from the route to their real names
	* eg: photos => Photos, my-photos => MyPhotos for controllers
	* 
	*/
	private function _dashToCamelCase($string, $lowerCaseFirstChar = false)
	{
		
		if (empty($string)) {
			throw new \Exception('Error: empty controller/action name was given.');
		}
		
		$newString = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
		if ($lowerCaseFirstChar) {
			$newString = lcfirst($newString); //lcfirst work with php 5.3
		}
		
		return $newString;
	}
	
}